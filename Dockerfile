FROM golang:1.13.8 AS builder

COPY main.go .

WORKDIR /go

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -ldflags="-w -s" -o main .

RUN ls;pwd

FROM scratch

# Copy the Pre-built binary file from the previous stage
# Set up the app to run as a non-user
# User ID 65534 is usually user 'nobody'
COPY --from=builder --chown=65534:0 /go/main ./main

USER 65534

EXPOSE 3000

ENTRYPOINT ["./main"]
